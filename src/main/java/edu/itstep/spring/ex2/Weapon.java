package edu.itstep.spring.ex2;

public interface Weapon {
    void shoot();
}
