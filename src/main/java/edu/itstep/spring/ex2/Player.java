package edu.itstep.spring.ex2;

import java.util.List;

public class Player {
    private String name;
    private int bullet;
    private List<Weapon> weapons;


    public void shoot(){
        if (bullet <= 0) {
            System.out.println("There are no cartridges =(");
            return;
        }

        System.out.println("before shoot: " + bullet);
        System.out.println(name + " take aim --->");
        int halfBullets = bullet / 2;
        if (bullet % 2 != 0) {
            halfBullets++;
        }
        for (int i = 0; i < weapons.size(); i++) {
            Weapon weapon = weapons.get(i);
            for (int j = 0; j < halfBullets; j++) {
                if (bullet > 0) {
                    weapon.shoot();
                    bullet--;
                    System.out.println("after shoot: " + bullet);
                }
            }
            if (i == 0) {
                halfBullets--;
            }
        }
    }



    public void setWeapons(List<Weapon> weapons) {
        this.weapons = weapons;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBullet(int bullet) {
        this.bullet = bullet;
    }
}
