package edu.itstep.spring.ex2;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext2.xml");
        Player player = context.getBean("player", Player.class);
        player.shoot();
    }
}
