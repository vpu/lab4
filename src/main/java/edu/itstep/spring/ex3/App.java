package edu.itstep.spring.ex3;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext3.xml");
        User user = (User) context.getBean("user");
        user.setFirstName("Ivan");
        user.setFirstName("Petr");
        user.setLastName("Ivanov");
    }
}
