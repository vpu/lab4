package edu.itstep.spring.ex3;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Aspect
public class LoggingAspect {
    @Around("execution(* User.set*(..))")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
        LocalTime start = LocalTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss:SSS");
        System.out.println("begin " + joinPoint.getSignature().getName().substring(3) + "() " + formatter.format(start));
        Object[] args = joinPoint.getArgs();
        for (Object arg : args) {
            System.out.println("data: " + arg);
        }
        Object proceed = joinPoint.proceed();
        LocalTime end = LocalTime.now();
        System.out.println("end " + joinPoint.getSignature().getName().substring(3) + "() " + formatter.format(end));
        for (int i = 0; i < 30; i++) {
            System.out.print("-");
        }
        System.out.println();
        return proceed;
    }
}
