package edu.itstep.spring.ex1;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext1.xml");
        Profile profile = context.getBean("profile", Profile.class);
        System.out.println("Name: " + profile.getName());
        System.out.println("Surname: " + profile.getSurname());
        System.out.println("Phone Number: " + profile.getPhoneNumber());
    }
}
